package com.knockmark.wimf.utils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.knockmark.wimf.R

fun createDialog(
        context: Context,
        message: String = "Message",
        okBtn: String = context.getString(android.R.string.ok),
        noBtn: String = "",
        isCancelable: Boolean = true,
        positiveListener: DialogInterface.OnClickListener? = null,
        negativeListener: DialogInterface.OnClickListener? = null): AlertDialog.Builder {
    val dialog = AlertDialog.Builder(context, R.style.BasicDialog)
            .setMessage(message)
            .setPositiveButton(okBtn, positiveListener)
            .setCancelable(isCancelable)
    if (noBtn.isNotEmpty()) {
        dialog.setNegativeButton(noBtn, negativeListener)
    }

    return dialog
}