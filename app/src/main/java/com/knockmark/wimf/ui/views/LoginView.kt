package com.knockmark.wimf.ui.views

import com.knockmark.wimf.base.BaseView
import com.knockmark.wimf.ui.login.model.LoginResponse

interface LoginView : BaseView {
    fun onLoginSucceeded(response: LoginResponse)
    fun onLoginFailed(throwable: Throwable)
}