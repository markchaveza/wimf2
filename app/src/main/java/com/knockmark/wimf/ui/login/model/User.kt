package com.knockmark.wimf.ui.login.model

import android.os.Bundle

data class User(var email: String,
                var password: String,
                var accessToken: String,
                var expireIn: String,
                var tokenType: String
) {
    val bundle: Bundle
        get() {
            val b = Bundle()
            b.putString("email", email)
            b.putString("password", password)
            b.putString("accessToken", accessToken)
            b.putString("expireIn", expireIn)
            b.putString("tokenType", tokenType)
            return b
        }
}