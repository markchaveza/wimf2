package com.knockmark.wimf.ui.home

import android.support.v4.app.Fragment
import com.knockmark.wimf.R
import com.knockmark.wimf.base.BaseActivity
import com.knockmark.wimf.ui.home.HomeFragment
import com.knockmark.wimf.ui.login.model.User

class HomeActivity : BaseActivity() {

    private var homeFragment: HomeFragment? = null
    private var token: String? = null

    override fun getFragment(): Fragment {
        homeFragment = HomeFragment()
        return homeFragment!!
    }

    override fun initView() {
        super.initView()
    }

    override fun getUser(user: User) {
        token = user.accessToken
        onResume()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.overridePendingTransition(R.anim.rigth_in, R.anim.rigth_out)
    }

}