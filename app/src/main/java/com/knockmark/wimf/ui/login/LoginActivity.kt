package com.knockmark.wimf.ui.login

import android.Manifest
import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import com.ia.mchaveza.kotlin_library.PermissionCallback
import com.ia.mchaveza.kotlin_library.PermissionManager
import kotlinx.android.synthetic.main.activity_base.*
import com.knockmark.wimf.R
import com.knockmark.wimf.account.AccountsManager
import com.knockmark.wimf.account.Authenticator
import com.knockmark.wimf.base.BaseActivityNoAuthentication
import com.knockmark.wimf.ui.home.HomeActivity
import com.knockmark.wimf.ui.login.model.User
import com.knockmark.wimf.utils.createDialog
import org.jetbrains.anko.design.longSnackbar

class LoginActivity : BaseActivityNoAuthentication(), PermissionCallback {

    private var mResultBundle: Bundle? = null
    private var loginFragment: LoginFragment? = null
    private var mAccountAuthenticatorResponse: AccountAuthenticatorResponse? = null
    private val permissionManager by lazy { PermissionManager(this, this) }

    override fun getFragment(): Fragment {
        loginFragment = LoginFragment()
        return loginFragment!!
    }

    override fun initView() {
        super.initView()
        setupAccountManager()
    }


    override fun finish() {
        if (this.mAccountAuthenticatorResponse != null) {
            // send the result bundle back if set, otherwise send an error.
            if (this.mResultBundle != null) {
                this.mAccountAuthenticatorResponse?.onResult(this.mResultBundle)
            } else {
                this.mAccountAuthenticatorResponse?.onError(AccountManager.ERROR_CODE_CANCELED,
                        "canceled")
            }
            this.mAccountAuthenticatorResponse = null
        }
        super.finish()
    }

    private fun setAccountAuthenticatorResult(result: Bundle) {
        this.mResultBundle = result
    }

    fun addLocalAccount(user: User) {
        if (permissionManager.permissionGranted(Manifest.permission.GET_ACCOUNTS)) {

            val accountManager = AccountManager.get(this)
            val accounts = accountManager.getAccountsByType(packageName)

            if (accounts.isNotEmpty() && AccountsManager.accountExistByName(user.email, accounts)) {
                val oldAccount = AccountsManager.getAccountByName(user.email, accounts)
                accountManager.setPassword(oldAccount, user.password)

                setAccountAuthenticatorResult(user.bundle)

                val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            } else {
                val account = Account(user.email, packageName)
                val userData = user.bundle
                if (accountManager.addAccountExplicitly(account, user.password, userData)) {
                    setAccountAuthenticatorResult(userData)
                    val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                } else {
                    longSnackbar(this.base_appbar, R.string.error_adding_account_messsage)
                }
            }
        } else {
            this.requestAccountPermissions(this, Authenticator.REQUEST_PERMISSIONS_CODE)
        }
    }

    private fun requestAccountPermissions(activity: Activity, requestCode: Int) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.GET_ACCOUNTS)) {
            createDialog(
                    context = this@LoginActivity,
                    message = getString(R.string.required_permissions_message),
                    okBtn = getString(R.string.accept),
                    noBtn = getString(R.string.cancel),
                    isCancelable = false,
                    positiveListener = DialogInterface.OnClickListener { dialog, _ ->
                        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.GET_ACCOUNTS), requestCode)
                        dialog.dismiss()
                    },
                    negativeListener = DialogInterface.OnClickListener { _, _ ->
                        finish()
                    }
            ).show()
        } else {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.GET_ACCOUNTS), requestCode)
        }
    }

    private fun setupAccountManager() {
        this.mAccountAuthenticatorResponse = intent.getParcelableExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE)
        if (this.mAccountAuthenticatorResponse != null) {
            this.mAccountAuthenticatorResponse?.onRequestContinued()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            Authenticator.REQUEST_PERMISSIONS_CODE -> {
                loginFragment?.login()
            }
        }
    }

    override fun onPermissionDenied(permission: String) {
        return Unit
    }

    override fun onPermissionGranted(permission: String) {
        return Unit
    }

}