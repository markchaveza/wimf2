package com.knockmark.wimf.ui.home

import com.knockmark.wimf.CleanApplication
import com.knockmark.wimf.R
import com.knockmark.wimf.base.BaseFragment

class HomeFragment : BaseFragment() {

    override fun getLayout(): Int =
            R.layout.fragment_home

    override fun initView() {
        super.initView()
        initializeDagger()
    }

    private fun initializeDagger() {
        CleanApplication.getApplicationComponent().inject(this)
    }

}