package com.knockmark.wimf.ui.login

import android.content.DialogInterface
import kotlinx.android.synthetic.main.fragment_login.*
import com.knockmark.wimf.CleanApplication
import com.knockmark.wimf.R
import com.knockmark.wimf.base.BaseFragment
import com.knockmark.wimf.ui.login.model.LoginResponse
import com.knockmark.wimf.ui.login.model.User
import com.knockmark.wimf.ui.presenters.LoginPresenter
import com.knockmark.wimf.ui.views.LoginView
import com.knockmark.wimf.utils.createDialog
import javax.inject.Inject

class LoginFragment : BaseFragment(), LoginView {

    @Inject
    lateinit var loginPresenter: LoginPresenter

    private lateinit var loginResponse: LoginResponse

    override fun getLayout(): Int =
            R.layout.fragment_login

    override fun initView() {
        super.initView()
        setupFragment()
        setListener()
    }

    private fun setListener() {
        this.login_btn.setOnClickListener {
            loginPresenter.login("mchaveza@ia.com.mx", "sky12.12")
        }
    }

    private fun setupFragment() {
        initializeDagger()
        loginPresenter.setViewReference(this)
    }

    private fun initializeDagger() {
        CleanApplication.getApplicationComponent().inject(this@LoginFragment)
    }

    override fun onLoginSucceeded(response: LoginResponse) {
        loginResponse = response
        (activity as LoginActivity).addLocalAccount(User("dummy@example.com",
                "dummyPassword",
                response.accessToken,
                response.expireIn.toString(),
                response.tokenType))
    }

    override fun onLoginFailed(throwable: Throwable) {
        createDialog(
                context = context!!,
                message = throwable.message.toString(),
                okBtn = getString(R.string.accept),
                positiveListener = DialogInterface.OnClickListener { dialog, _ ->
                    dialog.dismiss()
                }
        )
    }

    fun login() {
        (activity as LoginActivity).addLocalAccount(User("dummy@example.com",
                "dummyPassword",
                loginResponse.accessToken,
                loginResponse.expireIn.toString(),
                loginResponse.tokenType))
    }

    override fun showLoading() {
        loadingDialog.show(fragmentManager, LoginActivity::class.java.name)
    }

    override fun hideLoading() {
        loadingDialog.dismiss()
    }

    override fun onDetach() {
        super.onDetach()
        loginPresenter.stop()
    }

}