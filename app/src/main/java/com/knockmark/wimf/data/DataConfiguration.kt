package com.knockmark.wimf.data

class DataConfiguration(private val baseUrl: String) {

    fun getBaseUrl(): String = baseUrl

    companion object {
        const val LOGIN = "/ws/wsloginOrsan.php"
        const val CALL_ID = "OrsanAppClient"
        const val CALL_ID_KEY = "APPRGB8383-0ctl390#"
    }


}