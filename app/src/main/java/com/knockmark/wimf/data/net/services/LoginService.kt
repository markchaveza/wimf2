package com.knockmark.wimf.data.net.services

import com.knockmark.wimf.data.DataConfiguration
import com.knockmark.wimf.ui.login.model.LoginResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST
import rx.Observable

interface LoginService {

    @Headers("Content-Type: application/x-www-form-urlencoded; charset=UTF-8")
    @FormUrlEncoded
    @POST(DataConfiguration.LOGIN)
    fun login(
            @Field("CallId") callId: String,
            @Field("CallIdKey") callIdKey: String,
            @Field("usuario") user: String,
            @Field("pass") pass: String
    ): Observable<Response<LoginResponse>>

}