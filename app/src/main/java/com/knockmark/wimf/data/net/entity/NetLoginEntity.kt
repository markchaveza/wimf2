package com.knockmark.wimf.data.net.entity

import com.knockmark.wimf.data.net.services.LoginService
import com.knockmark.wimf.data.net.utils.ResponseUtils
import com.knockmark.wimf.data.repository.LoginRepository
import com.knockmark.wimf.ui.login.model.LoginRequest
import com.knockmark.wimf.ui.login.model.LoginResponse
import rx.Observable

class NetLoginEntity(private val loginService: LoginService) : LoginRepository {

    /**
     * We call LOGIN service to authenticate user
     * and we handle success and failure responses
     */
    override fun login(loginRequest: LoginRequest): Observable<LoginResponse> =
            loginService.login(loginRequest.callId, loginRequest.callIdKey, loginRequest.user, loginRequest.password)
                    .flatMap {
                        if (it.isSuccessful) {
                            if (it.body().status == 0) {
                                Observable.just(it.body())
                            } else {
                                Observable.error(ResponseUtils.processWrongBody(it))
                            }
                        } else {
                            Observable.error(ResponseUtils.processErrorResponse(it))
                        }
                    }

}