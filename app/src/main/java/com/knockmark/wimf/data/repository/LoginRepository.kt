package com.knockmark.wimf.data.repository

import com.knockmark.wimf.ui.login.model.LoginRequest
import com.knockmark.wimf.ui.login.model.LoginResponse
import rx.Observable

interface LoginRepository {

    fun login(loginRequest: LoginRequest): Observable<LoginResponse>

}