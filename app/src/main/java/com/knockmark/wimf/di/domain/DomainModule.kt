package com.knockmark.wimf.di.domain

import dagger.Module
import dagger.Provides
import com.knockmark.wimf.data.repository.LoginRepository
import com.knockmark.wimf.domain.LoginInteractor
import javax.inject.Singleton

@Module
class DomainModule {

    @Provides
    fun provideLoginInteractor(repository: LoginRepository) =
            LoginInteractor(repository)

}