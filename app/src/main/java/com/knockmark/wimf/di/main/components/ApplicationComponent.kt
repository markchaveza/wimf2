package com.knockmark.wimf.di.main.components

import com.knockmark.wimf.ui.login.LoginFragment
import dagger.Component
import com.knockmark.wimf.base.BaseActivity
import com.knockmark.wimf.base.BaseActivityNoAuthentication
import com.knockmark.wimf.base.BaseFragment
import com.knockmark.wimf.di.data.DataModule
import com.knockmark.wimf.di.domain.DomainModule
import com.knockmark.wimf.di.main.modules.ApplicationModule
import com.knockmark.wimf.ui.home.HomeFragment
import com.knockmark.wimf.ui.splash.SplashActivity
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class), (DataModule::class), (DomainModule::class)])
interface ApplicationComponent {
    fun inject(splashActivity: SplashActivity)
    fun inject(baseActivity: BaseActivity)
    fun inject(baseFragment: BaseFragment)
    fun inject(baseActivityNoAuthentication: BaseActivityNoAuthentication)
    fun inject(loginFragment: LoginFragment)
    fun inject(homeFragment: HomeFragment)
}