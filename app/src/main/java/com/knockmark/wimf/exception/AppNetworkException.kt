package com.knockmark.wimf.exception

class AppNetworkException : AppException {

    constructor(detailedMessage: String) : super(detailedMessage) {}

    constructor(detailedMessage: String, throwable: Throwable) : super(detailedMessage, throwable) {}

}