package com.knockmark.wimf.exception

class AppUnknownException : AppException {

    private var orsanResponseCode: Int = 0

    constructor(detailedMessage: String) : super(detailedMessage) {
        orsanResponseCode = -1
    }

    constructor(detailedMessage: String, throwable: Throwable) : super(detailedMessage, throwable) {
        orsanResponseCode = -1
    }

    constructor(detailedMessage: String, errorCode: Int) : super(detailedMessage) {
        orsanResponseCode = errorCode
    }

    constructor(detailedMessage: String, throwable: Throwable, errorCode: Int) : super(detailedMessage, throwable) {
        this.orsanResponseCode = errorCode
    }

    fun getResponseCode(): Int =
            this.orsanResponseCode

}