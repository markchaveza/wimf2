package com.knockmark.wimf.base

import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.MenuItem
import com.ia.mchaveza.kotlin_library.GeocoderManager
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.TrackingManager
import com.ia.mchaveza.kotlin_library.TrackingManagerLocationCallback
import kotlinx.android.synthetic.main.activity_base.*
import com.knockmark.wimf.CleanApplication
import com.knockmark.wimf.R
import com.knockmark.wimf.ui.dialogs.LoadingDialog
import javax.inject.Inject

abstract class BaseActivity : BaseAuthenticationActivity(), TrackingManagerLocationCallback {

    @Inject
    lateinit var preferences: SharedPreferencesManager

    @Inject
    lateinit var trackingManager: TrackingManager

    @Inject
    lateinit var geocoderManager: GeocoderManager

    lateinit var loadingDialog: LoadingDialog

    abstract fun getFragment(): Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        this.overridePendingTransition(R.anim.left_in, R.anim.left_out)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.base_container, getFragment())
                .commitAllowingStateLoss()

        initView()
    }

    open fun initView() {
        initializeDagger()
        prepareSupportActionBar()
        trackingManager.enablePermissionSetup(this)
        loadingDialog = LoadingDialog()
        loadingDialog.isCancelable = false
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.overridePendingTransition(R.anim.rigth_in, R.anim.rigth_out)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                this.onBackPressed()
                true
            }
            else -> false
        }
    }

    private fun initializeDagger() {
        CleanApplication.getApplicationComponent().inject(this)
    }

    private fun prepareSupportActionBar() {
        this.base_toolbar.title = ""
        setSupportActionBar(this.base_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.base_toolbar.navigationIcon = ContextCompat.getDrawable(this@BaseActivity, R.drawable.ic_back)
        this.base_toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onLocationHasChanged(location: Location) {
    }

    override fun onLocationHasChangedError(error: Exception) {
    }

}
