package com.knockmark.wimf.base

interface BaseView{
    fun showLoading()
    fun hideLoading()
}