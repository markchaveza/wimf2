package com.knockmark.wimf.base


open class BasePresenter<T : BaseView> {
    private var view: T? = null

    @Deprecated("Call setViewReference to avoid memory leaks.")
    fun setView(view: T?) {
        this.view = view
    }

    fun getView(): T? = this.view

}